require('dotenv').config()
var blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');

blurt.api.setOptions({ url: 'https://rpc.blurt.buzz', useAppbaseApi: true });

// interval in minutes
const CLAIM_INTERVAL = parseInt(process.env.CLAIM_INTERVAL || 360);

var accounts_json = require('./accounts.json')
const auto_claim_accounts = Object.keys(accounts_json);
if (_.isEmpty(auto_claim_accounts)) {
  console.log('empty auto_claim_accounts');
  process.exit(1);
}

setInterval(function() {
  blurt.api.getAccounts(auto_claim_accounts, function (err, result) {
    if (err) {
      console.log('get accounts error', err)
      process.exit(1);
    }
    // return if no auto claim setting
    if (_.isEmpty(result)) {
      console.log('empty account info');
      return;
    }

    result.forEach(account => {
      console.log('for account', account.name);
      let {
        name,
        reward_vesting_balance,
        reward_vesting_blurt,
      } = account;
      console.log('pending reward_vesting_balance', reward_vesting_balance);
      console.log('pending reward_vesting_blurt', reward_vesting_blurt);

      if (reward_vesting_balance !== '0.000000 VESTS') {
        let posting_key = accounts_json[account.name];

        console.log('claiming rewards...');
        blurt.broadcast.claimRewardBalance(
          posting_key, name, '0.000 BLURT', reward_vesting_balance,
          function(err, result) {
            console.log(err, result);
          }
        );

      }
    });

  });
}, (CLAIM_INTERVAL * 1000 * 60));